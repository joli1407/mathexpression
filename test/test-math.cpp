#include "catch.hpp"
#include "MathExpression.h"

TEST_CASE("Postfix/Infix functions"){
    MathExpression expr("4*6+5");

    SECTION("Infix"){
        REQUIRE(expr.infixNotation().compare("4*6+5") == 0);
    }

    SECTION("Postfix"){
        REQUIRE(expr.postfixNotation().compare("4 6 * 5 +"));
    }
}

TEST_CASE("Expression validity"){
    MathExpression expr("(3.0+2.0)-(2.0-1.0)");

    SECTION("Valid expression"){
        CHECK(expr.isValid());
        CHECK(expr.errorMessage().length() == 0);

        SECTION("Valid expresion - expression replaced with op="){
            expr = "5*(3+2)+2";
            CHECK(expr.isValid());
            CHECK(expr.errorMessage().length() == 0);

            SECTION("Valid expresion - expression replaced with op="){
                expr = "(3)-(2)";
                CHECK(expr.isValid());
                CHECK(expr.errorMessage().length() == 0);

                SECTION("Valid expresion - expression replaced with op="){
                    expr = "2";
                    CHECK(expr.isValid());
                    CHECK(expr.errorMessage().length() == 0);

                    SECTION("Valid expresion - expression replaced with op="){
                        expr = "(31)";
                        CHECK(expr.isValid());
                        CHECK(expr.errorMessage().length() == 0);
                    }
                }
            }
        }
    }

    SECTION("Invalid expresion - valid expr replaced with op="){
        expr = "HEY";
        CHECK(!expr.isValid());
        CHECK(expr.errorMessage().length() != 0);

        SECTION("Invalid expresion"){
            expr = "(3+4)(2+1)";
            CHECK(!expr.isValid());
            CHECK(expr.errorMessage().length() != 0);

            SECTION("Invalid expresion"){
                expr = "(2+3";
                CHECK(!expr.isValid());
                CHECK(expr.errorMessage().length() != 0);

                SECTION("Invalid expresion"){
                    expr = "2 1 3";
                    CHECK(!expr.isValid());
                    CHECK(expr.errorMessage().length() != 0);

                    SECTION("Invalid expresion"){
                        expr = "2++8";
                        CHECK(!expr.isValid());
                        CHECK(expr.errorMessage().length() != 0);
                    }
                }
            }
        }
    }
}

TEST_CASE("Calculation results"){
    MathExpression expr("3.5");

    SECTION("Lone number - decimals"){
        REQUIRE(expr.calculate() == 3.5);
    }

    SECTION("PLUS"){
        expr = "5+4";
        REQUIRE(expr.calculate() == 10.0);
    }

    SECTION("MINUS"){
        expr = "5-6";
        REQUIRE(expr.calculate() == -1.0);
    }

    SECTION("MULTI"){
        expr="2*6";
        REQUIRE(expr.calculate() == 12.0);
    }

    SECTION("DIV even"){
        expr = "9/3";
        REQUIRE(expr.calculate() == 3.0);
    }

    SECTION("DIV uneven"){
        expr = "1/3";
        REQUIRE(expr.calculate() == Approx(0.333));
    }

    SECTION("Parentheses"){
        expr = "(3+3)*3";
        REQUIRE(expr.calculate() == 18.0);
    }
}
